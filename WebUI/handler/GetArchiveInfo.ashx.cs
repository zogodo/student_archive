﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebUI.handler
{
    /// <summary>
    /// GetArchiveInfo 的摘要说明
    /// </summary>
    public class GetArchiveInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            Model.Student student = new Model.Student();
            student.stuid = context.Request["stuid"];
            student.name = context.Request["name"];
            student.gender = context.Request["gender"];

            DataTable tb = DAL.UserSever.GetInfoByNameAndStuid(student);

            //把从数据库中得到的数据拼接成html代码返回。
            string user_info_html = "";
            for (int i = 0; i < tb.Rows.Count; ++i)
            {
                user_info_html += @"
                用户名：" + tb.Rows[i]["stu_id"].ToString() +
                @"<br />姓名：" + tb.Rows[i]["name"].ToString() +
                @"<br />性别：" + tb.Rows[i]["stu_id"].ToString() +
                @" <br />email：" + tb.Rows[i]["stu_id"].ToString() +
                @" <br />联系号码：" + tb.Rows[i]["stu_id"].ToString() +
                @" <br />联系地址：" + tb.Rows[i]["stu_id"].ToString();
            }

            context.Response.Write(user_info_html);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}