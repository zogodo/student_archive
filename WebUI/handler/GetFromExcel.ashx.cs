﻿using SqlHelp;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebUI.handler
{
    /// <summary>
    /// GetFromExcl 的摘要说明
    /// </summary>
    public class GetFromExcl : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            /*/验证是否管理员
            if (context.Session["admin"] == null)
            {
                return;
            }*/

            //上传Excel文件
            HttpPostedFile file = context.Request.Files["file"];
            if (System.IO.Path.GetExtension(file.FileName) != ".xls")
            {
                context.Response.Write("请上传 .xls 文件！");
                return;
            }
            string errorMsg = null;
            string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName); //将文件名设置成guid值，避免重复
            string filepath = "/excl/" + filename; //生成文件路径
            //filename = GetMD5HashFromFile(file);
            string manpath = context.Server.MapPath(filepath); //用context.Server.MapPath()生成绝对路径
            try
            {
                file.SaveAs(manpath); //保存文件，SaveAs()方法需要绝对路径
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                context.Response.Write(errorMsg);
                return;
            }

            //进行Excel文件导入操作
            DataSet ds = ExecleDs(manpath, filename);
            DataRow[] dr = ds.Tables[0].Select();
            int rowsnum = ds.Tables[0].Rows.Count;
            if (rowsnum == 0)
            {
                context.Response.Write("Excel表为空表,无数据!");
                return;
            }
            else
            {
                for (int i = 0; i < dr.Length; i++)
                {
                    //try
                    //{
                        if (string.IsNullOrEmpty(dr[i]["学号"].ToString().Trim()))
                            break;

                        Model.Student stu_info = new Model.Student();
                        stu_info.stuid = dr[i]["学号"].ToString().Trim();
                        stu_info.name = dr[i]["姓名"].ToString().Trim();
                        stu_info.gender = dr[i]["性别"].ToString().Trim();
                        stu_info.college = dr[i]["学院"].ToString().Trim();
                        stu_info.marjor = dr[i]["专业"].ToString().Trim();
                        stu_info.send_time = dr[i]["寄送时间"].ToString().Trim();
                        stu_info.EMS_num = dr[i]["寄档机要号或EMS"].ToString().Trim();
                        stu_info.department = dr[i]["就业单位"].ToString().Trim();
                        stu_info.send_to = dr[i]["档案去向"].ToString().Trim();
                        stu_info.remark = dr[i]["备注"].ToString().Trim();

                        DAL.UserSever.ExcelToDB(stu_info);
                    //}
                    //catch (Exception e1)
                    //{
                    //    context.Response.Write("Excle表导入失败! " + e1.ToString());
                    //    return;
                    //}
                }
                context.Response.Write("Excle表导入成功!");
            }
        }

        //类似于数据库的打开连接，读取信息之后返回数据
        public DataSet ExecleDs(string filenameurl, string table)
        {
            string strConn = "Provider=Microsoft.Jet.OleDb.4.0;" + "data source=" + filenameurl + ";Extended Properties='Excel 8.0; HDR=YES; IMEX=1'";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataSet ds = new DataSet();
            OleDbDataAdapter odda = new OleDbDataAdapter("select * from [Sheet1$]", conn);
            odda.Fill(ds, table);
            conn.Close();
            return ds;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}