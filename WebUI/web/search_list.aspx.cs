﻿using System;
using System.Data;

namespace WebUI.web
{
    public partial class search_list : System.Web.UI.Page
    {
        public DataTable info = new DataTable("");

        protected void Page_Load(object sender, EventArgs e)
        {
            Model.Student student = new Model.Student();
            student.stuid = Request.QueryString["stuid"];
            student.name = Request.QueryString["name"];

            if (student.stuid == null || student.stuid == "")
            {
                info = DAL.UserSever.GetInfoByName(student);
            }
            else
            {
                info = DAL.UserSever.GetInfoByNameAndStuid(student);
            }
        }
    }
}