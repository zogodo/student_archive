﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="search_list.aspx.cs" Inherits="WebUI.web.search_list" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="../css/style.css" rel="stylesheet">
    <script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="../js/scroller_roll.js"></script>
    <title>毕业生档案查询系统</title>

</head>
<body onload="startTime()" style="width:1280px; margin:auto">
<div id="header">
    <img src="../images/guetlogo.png" class="logoImg">
    <div id="top_container">
        <h1><img src="../images/title.png"></h1>
        <div id="left_top">
            <ul class="ul1">
                <li style="width:200px;"><a href="#">桂林电子科技大学</a></li>
                <li style="width:250px;"><a href="#">桂林电子科技大学学工部（处）</a></li>
            </ul>
        </div>
        <div id="right_top">
            <ul class="ul2">
                <li style="width:150px;padding-right:0;"><a href="#">咨询留言</a></li>
                <li style="width:150px;"><a href="#">常见问题解答</a></li>
            </ul>
        </div>
        <div id="toknew">
            <p>毕业生档案一般于七月中旬通过EMS特快或机要寄到派遣证（报到证）上所写的单位或派遣证上单位的存档机构。</p>
            <p>通常需要1－2周时间到达，请各位同学耐心等待，如需查询请与派遣证（报到证）上所写的单位联系。 </p>
        </div>

    </div>
</div>
<div id="content">
    <div id="container">
        <table>
            <tr>
                <td class="tdOne1">学号</td>
                <td class="tdOne1" style="width: 57px;">姓名</td>
                <td class="tdOne1" style="width: 38px;">性别</td>
                <td class="tdOne1">学院</td>
                <td class="tdOne1">专业</td>
                <td class="tdOne1" style="width: 91px;">寄送时间</td>
                <td class="tdOne1" style="width: 136px;">寄档机要号或EMS</td>
                <td class="tdOne1">就业单位</td>
                <td class="tdOne1">档案去向</td>
                <td class="tdOne1" style="width: 91px;">备注</td>
            </tr>
            <%for (int i = info.Rows.Count - 1; i > -1; --i) { %>
            <tr>
                <td class="tdTwo1"><%= info.Rows[i]["stu_id"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["name"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["gender"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["college"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["marjor"].ToString() %></td>
                <td class="tdTwo1"><%=Convert.ToDateTime(info.Rows[i]["send_time"]).ToString("yyyy-MM-dd") %></td>
                <td class="tdTwo1"><%= info.Rows[i]["EMS_num"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["department"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["send_to"].ToString() %></td>
                <td class="tdTwo1"><%= info.Rows[i]["remark"].ToString() %></td>
            </tr>
            <%} %>
        </table>
        <div class="footer">
            <table>
                <tr>
                    <td class="td1">共<% = info.Rows.Count %>条记录</td>
                    <td class="td2">当前页1/1</td>
                    <td class="td3"><a href="">首页|</a></td>
                    <td class="td4"><a href="">下页|</a></td>
                    <td class="td5"><a href="">尾页|</a></td>
                    <td class="td6">转到第<select>
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>页</td>
                    <td class="td7">返 回</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div id="footer">
    <p>2010版权所有桂林电子科技大学 </p>
    <p>联 系 电 话 ： 0773-2291958 </p>


</div>
<script>
    function search() {
        var name = document.getElementById("name").value;
        var stuid = document.getElementById("stuid").value;
        location.href = "demand.aspx?name=" + name + "&stuid=" + stuid;
    }
</script>
</body>
</html>