﻿using System;

namespace Model
{
    public class Student
    {
        public string stuid{ get; set; }
        public string name{ get; set; }
        public string gender{ get; set; }
        public string college { get; set; }
        public string marjor { get; set; }
        public string send_time { get; set; }
        public string EMS_num { get; set; }
        public string department { get; set; }
        public string send_to { get; set; }
        public string remark { get; set; }
    }

    public class Comment
    {
        public string username { get; set; }   //评论者
        public string text { get; set; }    //评论内容
        public long page_id { get; set; }   //文章id
        public long id { get; set; }       //评论表id
        public DateTime time { get; set; }  //发表时间
    }
}
