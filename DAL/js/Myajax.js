﻿function AJAX(Url,Success,Fail)
{
    var xmlhttp;

    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                Success(xmlhttp.responseText);
            }
            else {
                fail(xmlhttp.status);
            }
        }
    }

    xmlhttp.open("POST", Url, true);
    xmlhttp.send();
}