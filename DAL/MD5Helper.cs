﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MD5Help
{
    public class MD5Helper
    {
        public static string GetMD5(string init)
        {
            if (init == "")
            {
                return init;
            }
            MD5 md5 = MD5.Create();

            byte[] src = Encoding.UTF8.GetBytes(init);
            byte[] desti = md5.ComputeHash(src);

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < desti.Length; i++)
            {
                result.Append(desti[i].ToString("x2").ToLower());
            }

            return result.ToString();
        }
    }
}
