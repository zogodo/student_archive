﻿using SqlHelp;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class UserSever
    {
        public static DataTable GetInfoByNameAndStuid(Model.Student student)
        {
            //查看用户信息
            DataTable userinfo = SqlHelper.ExecuteDataTable("select * from students where stu_id = @stuid and name=@name",
                                      new SqlParameter("@name", student.name),
                                      new SqlParameter("@stuid", student.stuid));
            return userinfo;
        }

        public static DataTable GetInfoByName(Model.Student student)
        {
            //查看用户信息
            DataTable userinfo = SqlHelper.ExecuteDataTable("select * from students where name=@name",
                                      new SqlParameter("@name", student.name));
            return userinfo;
        }

        public static void ExcelToDB(Model.Student stu_info)
        {
            SqlHelper.ExecuteNonQuery(@"INSERT INTO students(stu_id, name, gender, college, marjor, send_time, EMS_num, department, send_to, remark)
                                            VALUES(@stuid, @name, @gender, @college, @marjor, @send_time, @EMS_num, @department, @send_to, @remark)",
                        new SqlParameter("@stuid", stu_info.stuid),
                        new SqlParameter("@name", stu_info.name),
                        new SqlParameter("@gender", stu_info.gender),
                        new SqlParameter("@college", stu_info.college),
                        new SqlParameter("@marjor", stu_info.marjor),
                        new SqlParameter("@send_time", stu_info.send_time),
                        new SqlParameter("@EMS_num", stu_info.EMS_num),
                        new SqlParameter("@department", stu_info.department),
                        new SqlParameter("@send_to", stu_info.send_to),
                        new SqlParameter("@remark", stu_info.remark));
        }
    }
}
